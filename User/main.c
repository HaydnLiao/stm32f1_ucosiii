/**
  ******************************************************************************
  * @file    User/main.c 
  * @date    11-Dec-2017
  * @brief   Main program body
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "usart1_printf.h"
#include "systick_delay.h"
#include "includes.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
//	OS_ERR err;
	
	Systick_Delay_Init();
	Usart1_Printf_Init();

//	OSInit(&err);
//	OS_CRITICAL_ENTER();
//	OSTaskCreate(OS_TCB * p_tcb,
//				CPU_CHAR * p_name,
//				OS_TASK_PTR p_task,
//				void * p_arg,
//				OS_PRIO prio,
//				CPU_STK * p_stk_base,
//				CPU_STK_SIZE stk_limit,
//				CPU_STK_SIZE stk_size,
//				OS_MSG_QTY q_size,
//				OS_TICK time_quanta,
//				void * p_ext,
//				OS_OPT opt,
//				OS_ERR *p_err);
//	OSStart(&err);
//	OS_CRITICAL_EXIT();
	
	/* Infinite loop */
	while (1)
	{
		printf("OK\r\n");
		delay_ms(1000);
	}
}

//void start_task(void *p_arg)
//{
//	CPU_Init();
//}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
