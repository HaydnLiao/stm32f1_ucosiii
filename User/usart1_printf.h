/**
  ******************************************************************************
  * @file    User/usart1_printf.h 
  * @date    11-Dec-2017
  * @brief   all the functions prototypes for USART1
  ******************************************************************************
  */ 

#ifndef __USART1_PRINTF_H
#define __USART1_PRINTF_H

#include "stm32f10x.h"
#include <stdio.h>

void Usart1_Printf_Init(void);
static void Usart1_Config(void);
// static void NVIC_Config(void);

#endif
