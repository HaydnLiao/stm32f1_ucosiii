/**
  ******************************************************************************
  * @file    User/systick_delay.c 
  * @date    11-Dec-2017
  * @brief   Systick config functions and delay function
  ******************************************************************************
  */

#include "systick_delay.h"

static __IO uint32_t systickDT;

void Systick_Delay_Init(void)
{
	// 配置滴答定时器中断为1us
	while(SysTick_Config(SystemCoreClock / 1000000))
	{		
	}
	// 关闭滴答定时器
	SysTick->CTRL &= ~ SysTick_CTRL_ENABLE_Msk;
	// 设为最高优先级
	NVIC_SetPriority(SysTick_IRQn, 0);
}

void Delay_us(uint32_t delayedTime)
{ 
	systickDT = delayedTime;
	// 开启滴答定时器
	SysTick->CTRL |=  SysTick_CTRL_ENABLE_Msk;
	while(systickDT > 0)
	{
	}
	// 关闭滴答定时器
	SysTick->CTRL &= ~ SysTick_CTRL_ENABLE_Msk;
}

void SysTick_Handler(void)
{
	if(systickDT > 0)
	{
		systickDT -= 1;
	}
}
