/**
  ******************************************************************************
  * @file    User/usart1_printf.c 
  * @date    11-Dec-2017
  * @brief   USART1 config functions and send data function
  ******************************************************************************
  */  

#include "usart1_printf.h"

/**
  * @brief  初始化USART1
  * @param  None
  * @retval None
  */
void Usart1_Printf_Init(void)
{
	Usart1_Config();
	// NVIC_Config();
	// USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
}

/**
  * @brief  配置USART1引脚及串口功能
  * @param  None
  * @retval None
  */
static void Usart1_Config(void)
{
	GPIO_InitTypeDef gpioInit;
	USART_InitTypeDef usartInit;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_USART1, ENABLE);
	// USART1 Tx 复用推挽输出
	gpioInit.GPIO_Pin = GPIO_Pin_9;
	gpioInit.GPIO_Mode = GPIO_Mode_AF_PP;
	gpioInit.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &gpioInit);
	// USART1 Rx 浮空输入模式
	gpioInit.GPIO_Pin = GPIO_Pin_10;
	gpioInit.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &gpioInit);
	// 配置USART1
	usartInit.USART_BaudRate = 115200;
	usartInit.USART_WordLength = USART_WordLength_8b;
	usartInit.USART_StopBits = USART_StopBits_1;
	usartInit.USART_Parity = USART_Parity_No;
	usartInit.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usartInit.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &usartInit);
	USART_Cmd(USART1, ENABLE);
}
/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
int fputc(int ch, FILE *f)
{
  USART1->SR;
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART */
  USART_SendData(USART1, (uint8_t) ch);

  /* Loop until the end of transmission */
  while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
  {
	}
  return ch;
}
